import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/core/models/user.model';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  userId: number;
  userSelected: User;

  @Input() user: User;
  @Input() usersFromPageOne: User[];

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
    console.log('user compo : ', this.user);
  }

  onClick(user) {
    this.userId = user.id;
    console.log('user compo id : ', user.id);
    console.log(`user compo object id=${user.id} list : `, this.usersFromPageOne[this.userId - 1]);
    this.userSelected = this.usersFromPageOne[this.userId - 1];
    console.log('user selected : ', this.userSelected);
    this.userService.setData(this.userSelected);
    this.router.navigate(['userDetail', user.id]);
  }

}
