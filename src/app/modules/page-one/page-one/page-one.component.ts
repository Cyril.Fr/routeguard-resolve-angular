import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/core/models/user.model';
import { ApiCallService } from 'src/app/core/services/api-call.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-page-one',
  templateUrl: './page-one.component.html',
  styleUrls: ['./page-one.component.css']
})
export class PageOneComponent implements OnInit {

  public users: User[];

  constructor(private apiCallService: ApiCallService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    /* this.apiCallService.getAll().subscribe(
      res => {
        this.users = res;
        console.log('page-one : ', this.users);
      }); */
    this.activatedRoute.data.subscribe(
        (data: { users: User[]}) => {
        this.users = data.users;
      });
  }

}
