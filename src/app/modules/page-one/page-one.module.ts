import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageOneRoutingModule } from './page-one-routing.module';
import { PageOneComponent } from './page-one/page-one.component';
import { UserComponent } from './user/user.component';


@NgModule({
  declarations: [
    PageOneComponent,
    UserComponent
  ],
  imports: [
    CommonModule,
    PageOneRoutingModule
  ]
})
export class PageOneModule { }
