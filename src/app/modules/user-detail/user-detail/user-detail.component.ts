import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/core/models/user.model';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  userSelected: User;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.userSelected.subscribe(
      res => {
        console.log('userDetail : ', res);
        this.userSelected = res;
      },
      error => {
        console.log(error);
      }
    );
  }

}
