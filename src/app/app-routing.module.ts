import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './modules/login/login.component';
import { UserResolverService } from './core/services/user-resolver.service';
import { AuthGuard } from './core/route-guards/auth.guard';


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},

  { path: 'login', component: LoginComponent},

  { path: 'pageOne',
    loadChildren: () => import('./modules/page-one/page-one.module').then(m => m.PageOneModule),
    resolve: {users: UserResolverService},
    canActivate: [AuthGuard] },

  { path: 'pageTwo',
    loadChildren: () => import('./modules/page-two/page-two.module').then(m => m.PageTwoModule) },

  { path: 'userDetail/:id',
    loadChildren: () => import('./modules/user-detail/user-detail.module').then(m => m.UserDetailModule),
    // resolve: {user: UserDetailResolverService},
    canActivate: [AuthGuard] },

  {
    path: 'users/:id',
    loadChildren: () => import('./modules/user-detail/user-detail.module').then(m => m.UserDetailModule)
  },

  { path: '**', redirectTo: 'login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
