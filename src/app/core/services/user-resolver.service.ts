import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { User } from '../models/user.model';
import { Observable, empty } from 'rxjs';
import { ApiCallService } from './api-call.service';

@Injectable({
  providedIn: 'root'
})
export class UserResolverService implements Resolve<User[]> {

  constructor(private apiCallService: ApiCallService) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<User[]> {
    return this.apiCallService.getAll();
  }
}
