import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userSelected: BehaviorSubject<User> = new BehaviorSubject<User>(null);

  constructor() { }

  setData(data: any) {
    console.log('user service : ', data);
    this.userSelected.next(data);
  }
}
