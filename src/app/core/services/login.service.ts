import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  userName: BehaviorSubject<string> = new BehaviorSubject<string>('');

  constructor() { }

  setData(data: string) {
    console.log('login service : ', data);
    this.userName.next(data);
  }
}
