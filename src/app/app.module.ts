import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginModule } from './modules/login/login.module';
import { from } from 'rxjs';
import { RouterModule } from '@angular/router';
import { PageOneModule } from './modules/page-one/page-one.module';
import { PageTwoModule } from './modules/page-two/page-two.module';
import { UserDetailModule } from './modules/user-detail/user-detail.module';
import { HeaderComponent } from './core/components/header/header.component';
import { CoreModule } from './core/core.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LoginModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    PageOneModule,
    PageTwoModule,
    UserDetailModule,
    HttpClientModule,
    CoreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
